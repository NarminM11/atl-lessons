import java.util.Iterator;
import java.util.PriorityQueue;

public class Main {
    public static void main(String[] args) {

        PriorityQueue<String> animals = new PriorityQueue<>();

        animals.add("Dog");
        animals.add("Cat");
        animals.add("Horse");
        animals.add("Frog");

        //Methods:
        //iteratorMethod(animals);
        //offerMethod(animals);
        //clearMethod(animals);
        //sizeMethod(animals);
        peekMethod(animals);

    }

    public static void iteratorMethod(PriorityQueue<String> animals){
         // Write a Java program to iterate through all elements in priority queue
        Iterator<String> iterator =  animals.iterator();

        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    public static void offerMethod(PriorityQueue<String> animals){
        //Write a Java program to insert a given element into a priority queue
        animals.offer("Goat"); //The offer method inserts an element if possible, otherwise returning false. This differs from the Collection.add method, which can fail to add an element only by throwing an unchecked exception.
        System.out.println("Updated version: " + animals);
    }

    public static void clearMethod(PriorityQueue<String> animals){
        //Write a Java program to remove all the elements from a priority queue
       animals.clear();
        System.out.println(animals);
    }

    public static void sizeMethod(PriorityQueue<String> animals){
        //Write a Java program to count the number of elements in a priority queue
        int s = animals.size();
        System.out.println(s);
    }

    public static void peekMethod(PriorityQueue<String> animals){
        //Write a Java program to retrieve the first element of the priority queue

        System.out.println(animals.peek());
    }

    public static void change(PriorityQueue<String> animals){
        //Write a Java program to change priorityQueue to maximum priorityqueue



    }
}